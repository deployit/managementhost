Requirements
============

 * pika (https://github.com/pika/pika)
 * deployit common (https://git.math.uzh.ch/deployit/common)
 * web.py (http://webpy.org/)
 * Web Server (wsgi capable) or ...
 * wsgi application server


Reference Installation
======================

The reference installation uses nginx with uswgi.


Ansible Playbook
================

The ansible playbook deployit-managementhost.yml in the SVN repo
file:///scratch/svn/it/ansible/playbooks can be used to install the
management host.
