from distutils.core import setup
setup(name='managementhost',
      version='0.1a1',
      description='DeployIT Managementhost modules and scripts',
      author='Rafael Ostertag',
      author_email='support@math.uzh.ch',
      packages=['managementhost'],
      scripts=['bin/deployitlistener.py', 'bin/app_wsgi.py'])
