#!/usr/bin/env python

import argparse
import deployit.config
import deployit.rabbit
import deployit.utils
import json
import logging
import logging.handlers
import managementhost.database
import os
import signal
import sys
import time
import pika
import deployit.deployitlogging

rabbit = None
logger = None

def _signal_handler(signum, frame):
    if signum == signal.SIGTERM or\
       signum == signal.SIGQUIT or\
       signum == signal.SIGINT:
        shutdown()

def setup_argumentparser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--cfgfile',
                        help='configuration file',
                        default='/etc/deployit/listener.cfg',
                        required=False)
    parser.add_argument('--foreground',
                        help="don't fork to background",
                        action="store_const",
                        default=False,
                        const=True)

    return parser

def shutdown():
    rabbit.stop_consuming()

def setup_signal_handler():
    logger.debug('Setting up signal handlers')
    signal.signal(signal.SIGTERM, _signal_handler)
    signal.signal(signal.SIGQUIT, _signal_handler)
    signal.signal(signal.SIGINT, _signal_handler)

def process_messages(channel, method, header, body):
    if header.content_type != deployit.rabbit.MSG_CONTENT_TYPE:
        channel.basic_nack(delivery_tag=method.delivery_tag,
                           requeue=False)
        logger.error("Received message with content type '%s' instead of '%s'",
                     header.content_type,
                     deployit.rabbit.MSG_CONTENT_TYPE)
    else:
        channel.basic_ack(delivery_tag=method.delivery_tag)

    logger.debug('received message: %s', body)
    message = deployit.Message(body)

    database = managementhost.Database()
    database.add_resource_status(message.header['in-reply-to'],
                                 message.header['created-by'],
                                 message.header['message-creator']['module'],
                                 message.header['message-creator']['version'],
                                 message.header['message-creator']['modulepath'],
                                 message.header['created-on'],
                                 message.header['status-code'],
                                 message.body)

def run():
    global rabbit
    setup_signal_handler()

    keep_running = True
    retry_counter = 0
    while keep_running:
        try:
            rabbit = deployit.Rabbit()
            rabbit.consume_from_direct(deployit.rabbit.MANAGEMENTHOST_EXCHANGE,
                                       process_messages)

            logger.info('Start listening for messages')

            rabbit.start_consuming()
            keep_running = False
        except pika.exceptions.AMQPConnectionError:
            retry_counter = retry_counter + 1
            logger.warning("Connection error. Retrying (%d)...", retry_counter)
            time.sleep(deployit.config.listener['reconnect_interval'])
        except Exception as ex:
            logger.error("Error initializing rabbit: %s", repr(ex))
            return

    logger.debug("Stopped consuming messages")
    rabbit.stop_consuming()


if __name__ == "__main__":
    arguments = setup_argumentparser().parse_args()
    deployit.config.read(arguments.cfgfile)

    logger = logging.getLogger(deployit.deployitlogging.logger_name)
    logger.setLevel(deployit.utils.str_to_loglevel(deployit.config.listener['loglevel']))
    if not arguments.foreground:
        pid = os.fork()
        if pid != 0:
            # The parent
            sys.exit(0)

        # The child
        os.setsid()

        with open(deployit.config.listener['pidfile'], 'w') as pidfile:
            pidfile.write(str(os.getpid()))

        syslog_handler = logging.handlers.SysLogHandler(address='/dev/log',
                                                        facility='daemon')
        syslog_handler.setLevel(deployit.utils.str_to_loglevel(deployit.config.listener['loglevel']))
        formatter = logging.Formatter('%(name)s: %(message)s')
        syslog_handler.setFormatter(formatter)

        logger.addHandler(syslog_handler)


        logger.debug('Going to redirect stdin, stdout, stderr')
        dev_null_fd = os.open('/dev/null', os.O_RDWR)
        os.dup2(dev_null_fd, sys.stdin.fileno())
        os.dup2(dev_null_fd, sys.stdout.fileno())
        os.dup2(dev_null_fd, sys.stderr.fileno())

        os.close(dev_null_fd)
    else:
        ch = logging.StreamHandler()
        ch.setLevel(deployit.utils.str_to_loglevel(deployit.config.listener['loglevel']))
        logging.basicConfig(level=deployit.utils.str_to_loglevel(deployit.config.listener['loglevel']))
        logger.addHandler(ch)
        logger.debug('Stay in foreground')

    try:
        run()
    finally:
        if not arguments.foreground:
            logger.debug('Unlink pid file %s', deployit.config.listener['pidfile'])
            os.remove(deployit.config.listener['pidfile'])
