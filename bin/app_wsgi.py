import sys
try:
    import web
except ImportError as ex:
    print 'Unable to load module web: %s' % str(ex)

import deployit.modules
import deployit.config
import managementhost.rest
import managementhost.restmodules
import logging
import os

urls = ("/([a-zA-Z\d]+)/(.*)", managementhost.REST)

if 'DEPLOYITCONFIG' in os.environ:
    deployit.config.read(os.environ['DEPLOYITCONFIG'])
else:
    deployit.config.read('/opt/deployit/etc/deployit-managementhost.ini')
deployit.modules.init_modules(deployit.config.modules['path'])

app = web.application(urls, globals())
# that's for uwsgi
application = app.wsgifunc()
