import web
import deployit.rabbit
import deployit.message
import managementhost.restmodules
import logging
import json
import re
import deployit.deployitlogging

__author__ = 'Benjamin Baer <benjamin.baer@math.uzh.ch>'

logger = logging.getLogger(deployit.deployitlogging.logger_name)

class LogoutUser(managementhost.RestModule):
    """Implement the Logout User Module"""

    def __init__(self):
        super(LogoutUser, self).__init__('LogoutUser', 'logoutuser', 1)

    def get_endpoints(self):
        return { 'GET': r'(?P<ID>[a-f0-9-]+)',
                 'POST': None
             }

    @managementhost.restmodules.content_type_json
    def post_method(self, *args, **kwargs):
        try:
            client_data = json.loads(web.data())

            if not 'usernames' in client_data:
                raise web.BadRequest('usernames not defined')

            if not isinstance(client_data['usernames'],list):
                raise web.BadRequest('usernames not list')

            for username in client_data['usernames']:
                if not LogoutUser.VALIDITY_CHECK.match(username):
                    raise web.BadRequest('username not valid')

            message = self.create_message()
            message.body = client_data
            self.send_rabbit_message(message)
        except Exception as ex:
            logger.error(ex)
            raise web.InternalError(json.dumps({
                'module': self.name,
                'resource': self.resourcename,
                'version': self.version,
                'error': str(ex)
            }))

        raise web.Created(json.dumps({ 'uuid': message.header['uuid'] }))

    VALIDITY_CHECK = re.compile(r'^[\w.]+$')

def initialize():
    return LogoutUser().identification()

