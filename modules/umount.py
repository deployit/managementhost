import web
import deployit.rabbit
import deployit.message
import managementhost.restmodules
import logging
import json
import re
import deployit.deployitlogging

__author__ = 'Benjamin Baer <benjamin.baer@math.uzh.ch>'

logger = logging.getLogger(deployit.deployitlogging.logger_name)

class Umount(managementhost.RestModule):
    """Implement the umount Module"""

    def __init__(self):
        super(Umount, self).__init__('Umount', 'umount', 1)

    def get_endpoints(self):
        return { 'GET': r'(?P<ID>[a-f0-9-]+)',
                 'POST': None
             }

    @managementhost.restmodules.content_type_json
    def post_method(self, *args, **kwargs):
        try:
            client_data = json.loads(web.data())

            if not 'directory' in client_data:
                raise web.BadRequest('directory missing')

            if not Umount.VALIDITY_CHECK.match(client_data['directory']):
                raise web.BadRequest('directory not valid')

            message = self.create_message()
            message.body = client_data
            self.send_rabbit_message(message)
        except Exception as ex:
            logger.error(ex)
            raise web.InternalError(json.dumps({
                'module': self.name,
                'resource': self.resourcename,
                'version': self.version,
                'error': str(ex)
            }))

        raise web.Created(json.dumps({ 'uuid': message.header['uuid'] }))

    VALIDITY_CHECK = re.compile(r'^[\w\/.]+$')

def initialize():
    return Umount().identification()