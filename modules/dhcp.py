import web
import deployit.rabbit
import deployit.message
import managementhost.restmodules
import logging
import json
import deployit.deployitlogging

logger = logging.getLogger(deployit.deployitlogging.logger_name)

class Dhcp(managementhost.RestModule):
    """Implement the DHCP Module


    In the POST request JSON, it expects following structure:

    {
      "dhcp_include_file": <File to update>,
      "add": [ <list of MAC addresses to add> ],
      "remove": [ <list of MAC addresses to remove> ],
      "replace": <boolean indicating whether or not to replace file with contents from 'add'>
    }
    """

    def __init__(self):
        super(Dhcp, self).__init__('DHCP', 'dhcp', 1)

    def get_endpoints(self):
        return { 'GET': r'(?P<ID>[a-f0-9-]+)',
                 'POST': None
             }

    @managementhost.restmodules.content_type_json
    def post_method(self, *args, **kwargs):
        try:
            # try to make sense of the data sent by the client
            client_data = json.loads(web.data())

            # puny validation
            if not 'dhcp_include_file' in client_data or \
                    not 'replace' in client_data:
                raise web.BadRequest('data incomplete')

            if not 'add' in client_data and not 'remove' in client_data:
                raise web.BadRequest('data incomplete')

            message = self.create_message()
            message.body = client_data
            self.send_rabbit_message(message)
        except Exception as ex:
            logger.error(ex)
            raise web.InternalError(json.dumps({
                'module': self.name,
                'resource': self.resourcename,
                'version': self.version,
                'error': str(ex)
            }))

        raise web.Created(json.dumps({ 'uuid': message.header['uuid'] }))

def initialize():
    return Dhcp().identification()
