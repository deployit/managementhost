import web
import deployit.rabbit
import deployit.message
import managementhost.restmodules
import logging
import json
import re
import deployit.deployitlogging

__author__ = 'Rafael Ostertag <rafael.ostertag@math.uzh.ch>'

logger = logging.getLogger(deployit.deployitlogging.logger_name)

class UnlockScreensaver(managementhost.RestModule):
    """Implement the Unlock Screensaver Module"""

    def __init__(self):
        super(UnlockScreensaver, self).__init__('UnlockScreensaver', 'unlockscreensaver', 1)

    def get_endpoints(self):
        return { 'GET': r'(?P<ID>[a-f0-9-]+)',
                 'POST': None
             }

    @managementhost.restmodules.content_type_json
    def post_method(self, *args, **kwargs):
        try:
            client_data = json.loads(web.data())

            if not 'username' in client_data:
                raise web.BadRequest('data incomplete')

            if not UnlockScreensaver.VALIDITY_CHECK.match(client_data['username']):
                raise web.BadRequest('Malformed request')

            message = self.create_message()
            message.body = client_data
            self.send_rabbit_message(message)
        except Exception as ex:
            logger.error(ex)
            raise web.InternalError(json.dumps({
                'module': self.name,
                'resource': self.resourcename,
                'version': self.version,
                'error': str(ex)
            }))

        raise web.Created(json.dumps({ 'uuid': message.header['uuid'] }))

    VALIDITY_CHECK = re.compile(r'^[\w.]+$')

def initialize():
    return UnlockScreensaver().identification()

