import re
import web
import deployit.rabbit
import deployit.message
import managementhost.restmodules
import logging
import json
import deployit.deployitlogging

__author__ = 'Rafael Ostertag <rafael.ostertag@math.uzh.ch>'

logger = logging.getLogger(deployit.deployitlogging.logger_name)

class EmailList(managementhost.RestModule):
    """Implement the Email List Management Module

    Data has following format:

    {
      "lists": [
        { "operation": "create",
          "name": "<name>@<domain>",
          "displayname": "<text>"
        },
        { "operation": "delete",
          "name": "<name>@<domain>"
        }
      ],
      "members": [
        { "operation": "add",
          "name": "<name>@<domain>",
          "entries": [
            "<name1>@<domain>",
            "<name2>@<domain",
            "..."
          ]
        },
        { "operation": "remove",
          "name": "<name>@<domain>",
          "entries": [
            "<name1>@<domain>",
            "<name2>@<domain",
            "..."
          ]
        },
        { "operation": "set",
          "name": "<name>@<domain>",
          "entries": [
            "<name1>@<domain>",
            "<name2>@<domain",
            "..."
          ]
        }
      ]
    }

    * It is guaranteed, that "lists" operation are performed before "members" operations.
    * It is not required to have "lists" and "members" array present. Either will suffice.
    * displayname is optional

    """

    def __init__(self):
        super(EmailList, self).__init__('EmailList', 'emaillist', 1)

    def get_endpoints(self):
        return { 'GET': r'(?P<ID>[a-f0-9-]+)',
                 'POST': None
             }

    def _lists_op_valid(self, lists_ops_list):
        if not isinstance(lists_ops_list,list):
            return False

        for operation in lists_ops_list:
            if not isinstance(operation, dict):
                return False

            if 'operation' not in operation:
                return False

            if operation['operation'] not in ['create', 'delete']:
                return False

            if 'name' not in operation:
                return False

            if EmailList.LIST_NAME_RE.match(operation['name']) is None:
                return False

        return True

    def _members_op_valid(self, members_list):
        if not isinstance(members_list, list):
            return False

        for member_list in members_list:
            if not isinstance(member_list,dict):
                return False

            if 'operation' not in member_list:
                return False

            if member_list['operation'] not in ['add', 'remove', 'set']:
                return False

            if 'name' not in member_list:
                return False

            if EmailList.LIST_NAME_RE.match(member_list['name']) is None:
                return False

            if 'entries' not in member_list:
                return False

            if not isinstance(member_list['entries'],list):
                return False

            for entry in member_list['entries']:
                if EmailList.LIST_NAME_RE.match(entry) is None:
                    return False

        return True

    @managementhost.restmodules.content_type_json
    def post_method(self, *args, **kwargs):
        try:
            client_data = json.loads(web.data())

            # we require list and/or members in the request
            if not 'lists' in client_data and \
                not 'members' in client_data:
                raise web.BadRequest('data incomplete')

            if 'lists' in client_data:
                if not self._lists_op_valid(client_data['lists']):
                    raise web.BadRequest('data corrupt')

            if 'members' in client_data:
                if not self._members_op_valid(client_data['members']):
                    raise web.BadRequest('data corrupt')

            message = self.create_message()
            message.body = client_data
            self.send_rabbit_message(message)
        except web.BadRequest as ex:
            raise ex
        except Exception as ex:
            logger.error(ex)
            raise web.InternalError(json.dumps({
                'module': self.name,
                'resource': self.resourcename,
                'version': self.version,
                'error': str(ex)
            }))

        raise web.Created(json.dumps({ 'uuid': message.header['uuid'] }))


    LIST_NAME_RE = re.compile('[\d\w\.\-@]+')

def initialize():
    return EmailList().identification()
