"""Ping Module

The Ping Module is only a diagnostic module, identifying all Management Agents
"""

import web
import deployit.rabbit
import deployit.message
import managementhost.restmodules
import logging
import json
import deployit.deployitlogging

logger = logging.getLogger(deployit.deployitlogging.logger_name)

class Ping(managementhost.RestModule):
    """Implement the Ping Module"""

    def __init__(self):
        super(Ping, self).__init__('Ping', 'ping', 1)

    def get_endpoints(self):
        return { 'GET': r'(?P<ID>[a-f0-9-]+)',
                 'POST': None
             }

    @managementhost.restmodules.content_type_json
    def post_method(self, *args, **kwargs):
        try:
            message = self.create_message()
            message.body = "Ping"
            self.send_rabbit_message(message)
        except Exception as ex:
            logger.error(ex)
            raise web.InternalError(json.dumps({
                'module': self.name,
                'resource': self.resourcename,
                'version': self.version,
                'error': str(ex)
            }))

        raise web.Created(json.dumps({ 'uuid': message.header['uuid'] }))

def initialize():
    return Ping().identification()

