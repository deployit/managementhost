"""Generic Command Module
(not to be confused with the Apollo Command Module
http://en.wikipedia.org/wiki/Apollo_Command/Service_Module)
"""

import web
import deployit.rabbit
import deployit.message
import managementhost.restmodules
import logging
import json
import deployit.deployitlogging

logger = logging.getLogger(deployit.deployitlogging.logger_name)

class GenericCommand(managementhost.RestModule):
    """Implement the Generic Command Module"""

    def __init__(self):
        super(GenericCommand, self).__init__('GenericCommand', 'genericcommand', 1)

    def get_endpoints(self):
        return { 'GET': r'(?P<ID>[a-f0-9-]+)',
                 'POST': None
                 }

    @managementhost.restmodules.content_type_json
    def post_method(self, *args, **kwargs):
        try:
            # try to make sense of the data sent by the client
            client_data = json.loads(web.data())

            # puny validation
            for key in ('tag', 'command', 'arguments', 'useshell', 'capture_output'):
                if not key in client_data:
                    raise web.BadRequest('data incomplete: missing "%s"' % (key,))

            message = self.create_message()
            message.body = client_data
            self.send_rabbit_message(message)
        except Exception as ex:
            logger.error(ex)
            raise web.InternalError(json.dumps({
                'module': self.name,
                'resource': self.resourcename,
                'version': self.version,
                'error': str(ex)
            }))

        raise web.Created(json.dumps({ 'uuid': message.header['uuid'] }))

def initialize():
    return GenericCommand().identification()



