import managementhost.database
import deployit.config
import os
import uuid
import unittest
import logging

managementhost_log_fixtures = [
    ['1234-a', '19700101T00:00:00+00:00', 'localhost', 'module1', 1, 'nopath', 'POST', '{ "msg": Null }'],
    ['1234-b', '19700101T00:01:00+00:00', 'localhost1', 'module1', 1, 'nopath', 'DELETE', '{ "msg": Null }'],
    ['1234-c', '19700101T00:02:00+00:00', 'localhost', 'module2', 5, 'nopath2', 'DELETE', '{ "msg": Null }'],
    ['1234-d', '19700101T00:03:00+00:00', 'localhost3', 'module2', 5, 'nopath2', 'PUT', '{ "msg": Null }']
]

managementhost_log_invalid_fixtures = [
    ['1234-a', '19700101T00:00:00+00:00', 'localhost', 'module1', 1, 'nopath', 'PUT', '{ "msg": Null }'],
    ['1234-a', '19700101T00:01:00+00:00', 'localhost1', 'module2', 2, 'nopath', 'DELETE', '{ "msg": Null }'],
]
class TestDatabase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        deployit.config.read('testconfig.cfg')

    def setUp(self):
        try:
            os.remove(deployit.config.database['path'])
        except:
            pass

    def tearDown(self):
        self.setUp()

    def test_db_initialization(self):
        """Test the initialization of the Database"""

        db = managementhost.Database()

    def test_add_log(self):
        """Test add log"""
        db = managementhost.Database()
        [db.add_log(*arg) for arg in managementhost_log_fixtures]

    def test_add_invalid_log(self):
        """Test adding an invalid log

        The table managementhost_module must have a 'module1', but not
        a 'module2' because the fixture creating module2 has the same
        uuid as the fixture for module1 which must abort the
        transaction.

        """
        db = managementhost.Database()
        with self.assertRaises(Exception):
            [db.add_log(*arg) for arg in managementhost_log_invalid_fixtures]
        self.assertIsNone(db._get_host_module('module2', 2))

    def test_log_retrieval(self):
        db = managementhost.Database()
        [db.add_log(*arg) for arg in managementhost_log_fixtures]

        for fixture in managementhost_log_fixtures:
            db.get_log(fixture[0], fixture[3]) == fixture

    def test_get_module_logs(self):
        db = managementhost.Database()
        [db.add_log(*arg) for arg in managementhost_log_fixtures]

        logs = db.get_module_logs('module2', 5)
        self.assertEqual(len(logs), 2)


if __name__ == '__main__':
    logging.basicConfig(log_level=logging.DEBUG)
    unittest.main()
