""" Dummy Module doing nothing """

import managementhost.restmodules
import web
import uuid
import json

class Dummy2(managementhost.RestModule):
    def __init__(self):
        super(Dummy2, self).__init__('Dummy2', 'dummy2', 3)

        self.test_data = dict()

    def get_method(self, *args, **kwargs):
        web.header('Content-Type', 'application/json')
        if 'ID' not in kwargs:
            return json.dumps(self.test_data)

        if kwargs['ID'] not in self.test_data:
            raise web.NotFound()

        return json.dumps(self.test_data[kwargs['ID']])

    def post_method(self, *args, **kwargs):
        web.header('Content-Type', 'application/json')
        mid = uuid.uuid4()
        self.test_data[str(mid)] = json.loads(web.data())
        raise web.Created(json.dumps({ 'uuid': str(mid) }))

    def put_method(self, *args, **kwargs):
        web.header('Content-Type', 'application/json')
        return json.dumps({ 'arguments': args })

    def delete_method(self, *args, **kwargs):
        web.header('Content-Type', 'application/json')

        if 'ID' not in kwargs:
            raise web.NotFound()

        if kwargs['ID'] not in self.test_data:
            raise web.NotFound()

        try:
            del self.test_data[kwargs['ID']]
        except:
            raise web.InternalError()

        raise web.Accepted

    def head_method(self, *args, **kwargs):
        web.header('Content-Type', 'application/json')
        return json.dumps({ 'arguments': args })

    def get_endpoints(self):
        return { 'GET': r'(?P<ID>[\w\d-]+)',
                 'PUT': r'[0-9]+',
                 'DELETE': r'(?P<ID>[\w\d-]+)',
                 'POST': None,
                 'HEAD': r'(?P<ID>\d+)'}

def initialize():
    return Dummy2().identification()
