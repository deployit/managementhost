""" Dummy Module doing nothing """

import managementhost.restmodules

class Dummy(managementhost.RestModule):
    def __init__(self):
        super(Dummy, self).__init__('Dummy', 'dummy', 2)


def initialize():
    return Dummy().identification()
