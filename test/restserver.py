"""
This server has to run prior calling tests in rest.py
"""
try:
    import web
except ImportError as ex:
    print 'Unable to load module web: %s' % str(ex)

import deployit.modules
import deployit.config
import managementhost.rest
import managementhost.restmodules
import logging
import argparse
import sys

urls = ("/([a-zA-Z\d]+)/(.*)", managementhost.REST)
parser = argparse.ArgumentParser()
parser.add_argument('--cfgfile',
                    help='configuration file',
                    default='testconfig.cfg',
                    required=False)

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    args = parser.parse_args()

    deployit.config.read(args.cfgfile)
    deployit.modules.init_modules(deployit.config.modules['path'])

    # Remove command line argument, because they would be consumed by
    # app.run() as well. By doing so, we can't make the server
    # listening on a particular interface, but that's okay, since
    # we're testing.
    if len(sys.argv) == 3:
        del sys.argv[1:3]

    app = web.application(urls, globals())
    app.run()
