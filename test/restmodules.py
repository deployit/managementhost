#!/usr/bin/env python

import unittest
import deployit.modules
import deployit.config
import managementhost.restmodules
import os

class TestRestModules(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        deployit.config.read('testconfig.cfg')
        deployit.modules.init_modules(deployit.config.modules['path'])

    def test_existence(self):
        self.assertIn('Dummy', deployit.modules.modulelist)
        self.assertIn('dummy', deployit.modules.resourcelist)

        self.assertIn('Dummy2', deployit.modules.modulelist)
        self.assertIn('dummy2', deployit.modules.resourcelist)

    def test_module_modulelist_content(self):
        self.assertEqual(deployit.modules.modulelist['Dummy']['version'],2)
        self.assertEqual(deployit.modules.modulelist['Dummy']['module'],'Dummy')
        self.assertEqual(deployit.modules.modulelist['Dummy']['resource'],'dummy')
        self.assertEqual(deployit.modules.modulelist['Dummy']['path'],'fixtures/dummy.py')

        self.assertEqual(deployit.modules.modulelist['Dummy2']['version'],3)
        self.assertEqual(deployit.modules.modulelist['Dummy2']['module'],'Dummy2')
        self.assertEqual(deployit.modules.modulelist['Dummy2']['resource'],'dummy2')
        self.assertEqual(deployit.modules.modulelist['Dummy2']['path'],'fixtures/dummy2.py')

    def test_instance(self):
        module = deployit.modules.modulelist['Dummy']['instance']
        self.assertIsInstance(module, managementhost.RestModule)
        self.assertTrue(hasattr(module, 'get_endpoints'))
        self.assertTrue(hasattr(module, 'get_method'))
        self.assertTrue(hasattr(module, 'post_method'))
        self.assertTrue(hasattr(module, 'put_method'))
        self.assertTrue(hasattr(module, 'delete_method'))
        self.assertTrue(hasattr(module, 'head_method'))
        self.assertTrue(hasattr(module, 'options_method'))
        self.assertTrue(hasattr(module, 'identification'))


if __name__ == '__main__':
    unittest.main()
