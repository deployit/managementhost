#!/usr/bin/env python
"""
Please start restserver.py prior running the tests
"""
import unittest
import requests
import json
import logging

class TestREST(unittest.TestCase):
    url = 'http://localhost:8080/dummy2/'
    post_data = { "Name": "Ostertag",
                  "Firstname": "Rafael"
              }

    def test_get_fail1(self):
        r = requests.get('http://localhost:8080/dummy2')
        self.assertEqual(r.status_code, 404)

    def test_get_success(self):
        r = requests.get(TestREST.url)
        self.assertEqual(r.status_code, 200)

    def test_options(self):
        r = requests.options(TestREST.url)
        self.assertEqual(r.status_code, 200)
        self.assertIn('Allow', r.headers)
        self.assertIn('GET', r.headers['Allow'])
        self.assertIn('OPTIONS', r.headers['Allow'])
        self.assertIn('DELETE', r.headers['Allow'])
        self.assertIn('PUT', r.headers['Allow'])
        self.assertIn('POST', r.headers['Allow'])

    def test_post(self):
        r = requests.post(TestREST.url, json.dumps(TestREST.post_data))
        self.assertEqual(r.status_code, 201)
        response = json.loads(r.text)
        self.assertIn('uuid', response)

        p_id = response['uuid']

        r = requests.get(TestREST.url + p_id)
        self.assertEqual(r.status_code, 200)

        response = json.loads(r.text)
        self.assertIn('Name', response)
        self.assertIn('Firstname', response)
        self.assertEqual('Ostertag', response['Name'])
        self.assertEqual('Rafael', response['Firstname'])

        r = requests.delete(TestREST.url + p_id)
        self.assertEqual(r.status_code, 202)
        
        r = requests.get(TestREST.url + p_id)
        self.assertEqual(r.status_code, 404)
   

if __name__ == "__main__":
    unittest.main()
