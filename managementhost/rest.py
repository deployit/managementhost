""" Web.py application class """
import re
import deployit.modules
import managementhost.restmodules
import web
import logging
import sys

class REST(object):
    """Dispatcher for rest modules.

    Dispatch rest requests to registered rest modules.
    """
    def _get_module(self, resource, args, meth):
        # Locate the resource first. Remember, the resource is equal
        # to the module name
        if resource not in deployit.modules.resourcelist:
            raise web.NotFound()

        resource_instance = deployit.modules.resourcelist[resource]['instance']

        # get the endpoints list
        endpoints = resource_instance.get_endpoints()
        if meth not in endpoints:
            raise web.BadRequest()
            
        regex = endpoints[meth]
        if regex:
            match = re.match(regex, args)
        else:
            # No regex
            match = None

        return (resource_instance, match)

    def _call_instance_method(self, instance, match, meth):
        """ Call the appropriate instance method for 'meth'. It assumes that there exists such a method. """
        if hasattr(match,'groups'):
            args=match.groups()
        else:
            args=()

        if hasattr(match,'groupdict'):
            kwargs=match.groupdict()
        else:
            kwargs={}

        return getattr(instance, meth.lower() + "_method")(*args, **kwargs)

    def GET(self, resource, args):
        instance, match = self._get_module(resource, args, 'GET')
        return self._call_instance_method(instance, match, 'GET')

    def POST(self, resource, args):
        instance, match = self._get_module(resource, args, 'POST')
        return self._call_instance_method(instance, match, 'POST')

    def PUT(self, resource, args):
        instance, match = self._get_module(resource, args, 'PUT')
        return self._call_instance_method(instance, match, 'PUT')

    def DELETE(self, resource, args):
        instance, match = self._get_module(resource, args, 'DELETE')
        return self._call_instance_method(instance, match, 'DELETE')

    def HEAD(self, resource, args):
        instance, match = self._get_module(resource, args, 'HEAD')
        return self._call_instance_method(instance, match, 'HEAD')

    def OPTIONS(self, resource, args):
        if resource not in deployit.modules.resourcelist:
            raise web.NotFound()
        web.header('Allow', deployit.modules.resourcelist[resource]['instance'].options_method())
        return ""
        
