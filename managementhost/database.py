"""Database module"""

import deployit.config
import deployit.deployitlogging
import logging
try:
    import sqlite3
except ImportError as ex:
    print 'Unable to import sqlite3: ' + str(ex)

logger = logging.getLogger(deployit.deployitlogging.logger_name)

class Database(object):
    """Class representing Management Host database

    Before instantiating this class, make sure deployit.config.read()
    has been called.

    """

    def __init__(self):
        self.connection = sqlite3.connect(deployit.config.database['path'])

        # power on foreign key constraints
        self.connection.execute('PRAGMA foreign_keys = ON')

        # power on row factory
        self.connection.row_factory = sqlite3.Row

        if not self._table_exists('managementagent'):
            self._create_agent_table()

        if not self._table_exists('managementhost_module'):
            self._create_managementhost_module_table()

        if not self._table_exists('managementagent_module'):
            self._create_managementagent_module_table()

        if not self._table_exists('log'):
            self._create_log_table()

        if not self._table_exists('resource_status'):
            self._create_resource_status_table()

    def __enter__(self):
        return self

    def __exit__(self, exec_type, exec_value, traceback):
        self.connection.close()

    def add_log(self, uuid, submitted, origin, module, module_version,
                module_path, method, message):
        try:
            host_module = self._get_host_module(module,
                                                module_version)
            if host_module is None:
                self._create_host_module(module, module_version, module_path)
                host_module = self._get_host_module(module,
                                                    module_version)

            self._create_log(uuid, submitted, origin, host_module[0],
                             method, message)
        except Exception as ex:
            logger.error('Error while adding log to database: %s', str(ex))
            self.connection.rollback()
            raise

        self.connection.commit()

    def update_log_method(self, uuid, method):
        try:
            self.connection.execute("""
            UPDATE log SET method = ? WHERE uuid = ?
            """, (method, uuid))
        except Exception as ex:
            logger.error("Error while updating method of log '%s'", uuid)
            self.connection.rollback()
            raise

        self.connection.commit()

    def get_log(self, uuid, module):
        curs = self.connection.execute("""
        SELECT log.uuid AS 'uuid', log.submitted AS 'submitted',
          log.origin AS 'origin', managementhost_module.name AS 'module',
          log.method AS 'method', log.message AS 'message',
          managementhost_module.version AS 'version',
          managementhost_module.path AS 'path'
        FROM log, managementhost_module
        WHERE log.uuid=? AND
          log.module_id = managementhost_module.id AND
          managementhost_module.name = ?
        """, (uuid, module))

        data = curs.fetchone()
        if data is None:
            return None

        return dict(zip(data.keys(),data))

    def get_module_logs(self, module, version):
        curs = self.connection.execute("""
        SELECT log.uuid AS 'uuid', log.submitted AS 'submitted',
          log.origin AS 'origin', managementhost_module.name AS 'module',
          log.method AS 'method', log.message AS 'message',
          managementhost_module.version AS 'version',
          managementhost_module.path AS 'path'
        FROM log, managementhost_module
        WHERE log.module_id = managementhost_module.id AND
          managementhost_module.name = ? AND
          managementhost_module.version = ?
        """, (module, version))

        data = curs.fetchall()
        if data is None:
            return None

        return tuple([dict(zip(record.keys(),record)) for record in data])

    def get_resource_status(self, uuid, module):
        curs = self.connection.execute("""
        SELECT l.submitted AS 'submitted',
               rs.received AS 'received',
               a.name AS 'agentname',
               am.name AS 'module',
               am.version AS 'version',
               am.path AS 'path',
               rs.status_code AS 'status_code',
               rs.status_reason AS 'reason'
        FROM resource_status AS 'rs',
             log AS 'l',
             managementagent AS 'a',
             managementagent_module AS 'am'
        WHERE l.uuid = ? AND
              rs.log_id = l.id AND
              rs.agent_id = a.id AND
              rs.agent_module_id = am.id AND
              am.name = ?
        """, (uuid, module))

        data = curs.fetchall()
        if data is None:
            return None

        return tuple([dict(zip(record.keys(),record)) for record in data])

    def add_resource_status(self, replyto,
                            agentname, module,
                            module_version,
                            module_path,
                            received,
                            status_code,
                            reason):
        try:
            agent_module = self._get_agent_module(module, module_version)
            if agent_module is None:
                self._create_agent_module(module, module_version, module_path)
                agent_module = self._get_agent_module(module, module_version)

            agent = self._get_agent(agentname)
            if agent is None:
                self._create_agent(agentname)
                agent = self._get_agent(agentname)

            log_id = self._get_log_id_by_uuid(replyto)
            if log_id is None:
                logger.error("Unable to find log entry for %s", replyto)
                raise RuntimeError("Unable to find log entry for %s" % (replyto,))

            self._create_status_log(log_id, agent[0], agent_module[0], received, status_code, reason)
        except Exception as ex:
            logger.error('Error while resource status to database: %s', str(ex))
            self.connection.rollback()
            raise

        self.connection.commit()

    def _get_log_id_by_uuid(self, uuid):
        curs = self.connection.execute("""
        SELECT id FROM log WHERE uuid = ?
        """, (uuid,))

        data = curs.fetchone()
        if data is None:
            return None

        return data['id']

    def _create_host_module(self, name, version, path):
        self.connection.execute("""
        INSERT INTO managementhost_module
        (name,version, path)
        VALUES (?,?,?)
        """, (name, version, path))

    def _get_host_module(self, name, version):
        curs = self.connection.execute("""
        SELECT id, name, version, path
        FROM managementhost_module
        WHERE name=? AND version=?
        """, (name, version))

        data = curs.fetchone()
        if data is None:
            return None

        return tuple(data)

    def _create_agent_module(self, name, version, path):
        self.connection.execute("""
        INSERT INTO managementagent_module
        (name,version, path)
        VALUES (?,?,?)
        """, (name, version, path))

    def _get_agent_module(self, name, version):
        curs = self.connection.execute("""
        SELECT id, name, version, path
        FROM managementagent_module
        WHERE name=? AND version=?
        """, (name, version))

        data = curs.fetchone()
        if data is None:
            return None

        return tuple(data)

    def _create_agent(self, name):
        self.connection.execute("""
        INSERT INTO managementagent
        (name)
        VALUES (?)
        """, (name,))

    def _get_agent(self, name):
        curs = self.connection.execute("""
        SELECT id, name
        FROM managementagent
        WHERE name=?
        """, (name,))

        data = curs.fetchone()
        if data is None:
            return None

        return tuple(data)

    def _create_log(self, uuid, submitted, origin, module_id, method, message):
        self.connection.execute("""
        INSERT INTO log
        (uuid,submitted,origin,module_id,method, message)
        VALUES (?,?,?,?,?,?)
        """, (uuid, submitted, origin, module_id, method, message))

    def _create_status_log(self, log_id, agent_id,
                           agent_module_id,
                           received,
                           status_code,
                           status_reason):
        self.connection.execute("""
        INSERT INTO resource_status
        (log_id,agent_id,agent_module_id,received,status_code,status_reason)
        VALUES (?,?,?,?,?,?)
        """, (log_id, agent_id, agent_module_id, received, status_code, status_reason))

    def _table_exists(self, name):
        curs = self.connection.execute("SELECT name FROM sqlite_master WHERE type='table' AND name=?", (name,))
        return curs.fetchone() is not None

    def _create_log_table(self):
        self.connection.execute("""
        CREATE TABLE log (
           id INTEGER PRIMARY KEY AUTOINCREMENT,
           uuid TEXT NOT NULL UNIQUE, -- UUID of the request
           submitted TEXT NOT NULL,   -- date request has been submitted
           origin TEXT NOT NULL,      -- origin IP address of the submitting host
           module_id INTEGER NOT NULL,-- host module id
           method TEXT DEFAULT NULL,  -- HTTP request method
           message TEXT NOT NULL,     -- message received from origin host
           CONSTRAINT log_module_id_fkey FOREIGN KEY (module_id) REFERENCES managementhost_module (id) ON UPDATE RESTRICT ON DELETE RESTRICT
         )
        """)
        self.connection.commit()

    def _create_managementhost_module_table(self):
        self.connection.execute("""
        CREATE TABLE managementhost_module (
           id INTEGER PRIMARY KEY AUTOINCREMENT,
           name TEXT NOT NULL,      -- name of the host module
           version TEXT NOT NULL,   -- version of the host module
           path TEXT NOT NULL,      -- path where module has been loaded from
           CONSTRAINT managementagent_module_unique UNIQUE (name, version)
        )
        """)
        self.connection.commit()

    def _create_managementagent_module_table(self):
        self.connection.execute("""
        CREATE TABLE managementagent_module (
           id INTEGER PRIMARY KEY AUTOINCREMENT,
           name TEXT NOT NULL,     -- name of the agent module
           version TEXT NOT NULL,  -- version of the agent module
           path TEXT NOT NULL,     -- path where module has been loaded from
           CONSTRAINT managementagent_module_unique UNIQUE (name, version)
        )
        """)
        self.connection.commit()

    def _create_agent_table(self):
        self.connection.execute("""
        CREATE TABLE managementagent (
           id INTEGER PRIMARY KEY AUTOINCREMENT,
           name TEXT NOT NULL -- host name of the management agent
        )
        """)
        self.connection.commit()

    def _create_resource_status_table(self):
        self.connection.execute("""
        CREATE TABLE resource_status (
           log_id INTEGER,          -- log record id
           agent_id INTEGER,        -- id of the management host
           agent_module_id INTEGER, -- id of the management host module
           received TEXT NOT NULL,  -- status received date
           status_code INTEGER,     -- status code
           status_reason TEXT,      -- reason for the code
           CONSTRAINT resource_status_log_id_fkey FOREIGN KEY (log_id) REFERENCES log (id) ON UPDATE RESTRICT ON DELETE RESTRICT,
           CONSTRAINT resource_agent_id_fkey FOREIGN KEY (agent_id) REFERENCES managementagent (id) ON UPDATE RESTRICT ON DELETE RESTRICT
        )
        """)
        self.connection.commit()
