""" Base class for Rest Modules"""

import deployit.modules
import deployit.config
import deployit.message
import managementhost.database
import web
import logging
import string
import json
import deployit.deployitlogging

logger = logging.getLogger(deployit.deployitlogging.logger_name)

def content_type_json(f):
    """Decorator setting the HTTP content type to json"""
    def _decorator(*args, **kwargs):
        web.header('Content-Type', 'application/json')
        return f(*args, **kwargs)

    return _decorator

class RestModule(deployit.Module):
    """Base class for Management Host

    The resourcename is used as endpoint for REST and Rabbit.

    It will also open a connection to the Management Host Database.

    It is required to have deployit.config initialized before
    instantiating REST modules.

    Because this class uses managementhost.database, the function
    deployit.config.read() has to be called prior instantiation.

    """

    def __init__(self, name, resourcename, version):
        super(RestModule, self).__init__(name, resourcename, version)

    def get_endpoints(self):
        """
        Return a dictionary of supported HTTP Requests.

        This will also be used to implement the OPTIONS method (self.options())
        """
        raise NotImplementedError

    @content_type_json
    def get_method(self, *args, **kwargs):
        if 'ID' in kwargs:
            log = self.get_log_from_database(kwargs['ID'])
            if not log:
                raise web.NotFound(json.dumps({ 'uuid': kwargs['ID'] }))
            return json.dumps(
                {
                    'log': log,
                    'resource_status': self.get_resource_status_from_database(kwargs['ID'])
                }
            )
        else:
            return json.dumps(self.get_module_logs())

    def post_method(self, *args, **kwargs):
        raise NotImplementedError

    def put_method(self, *args, **kwargs):
        raise NotImplementedError

    def delete_method(self, *args, **kwargs):
        raise NotImplementedError

    def head_method(self, *args, **kwargs):
        raise NotImplementedError

    def options_method(self):
        return "OPTIONS " + string.join(self.get_endpoints().keys(), " ")

    def create_message(self):
        """Create a new message object and add headers"""
        msg = self.message_template()
        msg.header['rest-client'] = web.ctx.get('ip')
        return msg

    def send_rabbit_message(self, message):
        self.log_to_database(message)
        # Because Pika is not thread safe, and it is not
        # recommended to create a connection in one thread and use
        # that connection in another thread, we have to create the
        # connection here to satisfy this constraint.
        #
        # Also remember, that the deployit.rabbit.Rabbit class
        # takes the configuration of the rabbit server directly
        # from deployit.config. So, deployit.config.read() has to
        # be called somewhere before this method will be used.
        with deployit.Rabbit() as rabbit:
            rabbit.publish_to_fanout(self.resourcename,
                                     message)


    def log_to_database(self, message):
        # We have to open the database here because we might run
        # multithreaded and the database connection is only valid
        # in the thread it was created. This might be a
        # performance hog.
        #
        # TODO: Store HTTP request method in log record.
        with managementhost.database.Database() as database:
            database.add_log(message.header['uuid'],
                             message.header['created-on'],
                             web.ctx.get('ip'),
                             self.name,
                             self.version,
                             deployit.modules.modulelist[self.name]['path'],
                             None,
                             str(message))

    def get_log_from_database(self, uuid):
        # We have to open the database here because we might run
        # multithreaded and the database connection is only valid
        # in the thread it was created. This might be a
        # performance hog.
        with managementhost.database.Database() as database:
            return database.get_log(uuid, self.name)

    def get_resource_status_from_database(self, uuid):
        # We have to open the database here because we might run
        # multithreaded and the database connection is only valid
        # in the thread it was created. This might be a
        # performance hog.
        with managementhost.database.Database() as database:
            return database.get_resource_status(uuid, self.name)

    def get_module_logs(self):
        # We have to open the database here because we might run
        # multithreaded and the database connection is only valid
        # in the thread it was created. This might be a
        # performance hog.
        with managementhost.database.Database() as database:
            return database.get_module_logs(self.name, self.version)
